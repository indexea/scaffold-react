# scaffold-react

## 介绍

React  集成 indexea 搜索的脚手架

## 软件架构

React + Typescript + Vite

## 接口返回搜索组件的布局

```xml
<layout>
    <row>
        <col width="6">
            <control type="input" button="true" placeholder="搜索关键字" suggest="true"/>
        </col>
        <col width="6">
            <control type="hotwords"/>
        </col>
    </row>
    <row>
        <col width="9">
            <control type="indices"/>
        </col>
    </row>
    <row>
        <col width="9">
            <control type="results"/>
        </col>
        <col width="3">
            <control type="aggregation"/>
        </col>
    </row>
</layout>
```

## 安装教程

```bash
yarn install
yarn dev
yarn build
```
