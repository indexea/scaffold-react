import { SyntheticEvent } from 'react'
import { SearchContext, SearchFilter } from '@indexea/sdk/context'
import i18n from '../i18n'

export type AggregationProps = {
  context: SearchContext
  attrs: any
  name: string
  agg: any
  addFilter: (f: SearchFilter) => void
}

/**
 * 显示单个聚合信息
 * @param context
 * @param attrs
 * @param name
 * @param agg
 * @param addFilter
 * @returns
 */
export default function Aggregation ({
  context,
  attrs,
  name,
  agg,
  addFilter
}: AggregationProps) {
  const isArray = Array.isArray(agg.buckets)
  const multiple = (attrs['multiples'] || '').split(',').includes(name)

  if (agg.buckets.length < 1) return null

  return (
    <div className='card mt-3'>
      <div className='card-header'>
        <h6 className='mb-0'>{name}</h6>
      </div>
      <div className='card-body p-0'>
        {isArray && (
          <ul className='list-group list-group-flush'>
            {agg.buckets.map((bucket: any) => (
              <li
                className={
                  isSelected(bucket.key)
                    ? 'list-group-item d-flex justify-content-between align-items-start selected'
                    : 'list-group-item d-flex justify-content-between align-items-start'
                }
                key={bucket.key}
              >
                <div className='me-auto'>
                  <a
                    href={'?' + name + '=' + bucket.key}
                    onClick={e => filter(e, bucket.key)}
                  >
                    {bucket.key_as_string || bucket.key || i18n('empty_tag')}
                  </a>
                </div>
                <span className='badge bg-light text-dark fw-lighter'>
                  {bucket.doc_count}
                </span>
              </li>
            ))}
            {agg.sum_other_doc_count > 0 && (
              <li className='list-group-item d-flex justify-content-between align-items-start'>
                <div className='me-auto text-muted'>{i18n('other')}</div>
                <span className='badge bg-light text-dark fw-lighter'>
                  {agg.sum_other_doc_count}
                </span>
              </li>
            )}
          </ul>
        )}
        {!isArray && (
          <ul className='list-group list-group-flush'>
            {Object.entries(agg.buckets).map((bucket: any) => (
              <li
                className='list-group-item d-flex justify-content-between align-items-start'
                key={bucket[0]}
              >
                <div className='me-auto'>{bucket[0] || i18n('empty_tag')}</div>
                <span className='badge bg-light text-dark fw-lighter'>
                  {bucket[1]}
                </span>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  )

  function filter (e: SyntheticEvent, value: string) {
    e.preventDefault()
    let filter = new SearchFilter(name, value, agg?.interval, multiple)
    addFilter(filter)
  }

  function isSelected (key: string) {
    return context.filters.find(f => f.name === name && f.value == key)
  }
}
