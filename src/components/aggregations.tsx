import { SearchContext, SearchFilter } from '@indexea/sdk/context'
import Aggregation from './aggregation'

export type AggregationsProps = {
  context: SearchContext
  attrs: any
  aggregations: any
  addFilter: (f: SearchFilter) => void
}

/**
 * 显示所有的聚合信息
 * @param context
 * @param attrs
 * @param aggregations
 * @param addFilter
 * @returns
 */
export default function Aggregations ({
  context,
  attrs,
  aggregations,
  addFilter
}: AggregationsProps) {
  const aggs = Object.entries(aggregations || {})

  return (
    <div className='aggregations'>
      {aggs.map(([name, agg]) => (
        <Aggregation
          context={context}
          attrs={attrs}
          name={name}
          agg={agg}
          addFilter={addFilter}
          key={name}
        />
      ))}
    </div>
  )
}
