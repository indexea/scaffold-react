export default function (props: any) {
  return props.msg ? (
    <div
      className='d-flex justify-content-center align-middle'
      style={{ height: '40vh' }}
    >
      <div className='m-auto text-danger'>{props.msg}</div>
    </div>
  ) : null
}
