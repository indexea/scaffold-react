import { WidgetLayout } from '@indexea/sdk/layout'
import { SearchContext } from '@indexea/sdk/context'

import Input from './input'
import Hotwords from './hotwords'

export type HomeProps = {
  context: SearchContext
  search: (q: string) => void
}

/**
 * 搜索脚手架的首页
 * 包含 Logo、输入框以及热门搜索词
 * @param context
 * @returns
 */
export default function ({ context, search }: HomeProps) {
  //@ts-ignore
  const logo: string = context?.widget?.settings?.logo || '/static/indexea.png'
  const layout = WidgetLayout.parse(context.widget.layout || '<layout/>')
  return (
    <div
      className='d-flex justify-content-center align-middle'
      style={{ height: '60vh' }}
    >
      <div className='m-auto' style={{ width: '600px' }}>
        <div className='mb-4'>
          <img src={logo} className='mx-auto d-block' alt='...' width='200' />
        </div>
        <Input
          context={context}
          showLogo={false}
          attrs={layout.getAttributes('input')}
          search={search}
        />
        <Hotwords
          context={context}
          attrs={layout.getAttributes('hotwords')}
          search={search}
        />
      </div>
    </div>
  )
}
