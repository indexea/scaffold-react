import { useEffect, useState, SyntheticEvent } from 'react'
import { SearchWord } from '@indexea/sdk'
import { SearchContext } from '@indexea/sdk/context'
import config from '../../config.json'
import { Indexea } from '../openapi'

const indexea = new Indexea(config.api, '', config.widget)

export type HotwordsProps = {
  context: SearchContext
  attrs: any
  search: (q: string) => void
}

/**
 * 显示搜索热词
 * @param context
 * @param attrs
 * @param search
 * @returns
 */
export default function Hotwords ({ context, attrs, search }: HotwordsProps) {
  const query: number = context.query
  const count: number = attrs.show_count || 5
  const [words, setWords] = useState<SearchWord[]>([])

  useEffect(() => {
    indexea.hotwords(query, 'year', count).then(setWords)
  }, [query])

  return (
    <ul className='nav'>
      {words.map(word => (
        <li className='nav-item' key={word.q}>
          <a
            className='nav-link m-0 px-2'
            href={'?q=' + word.q}
            onClick={e => doSearch(e, word.q || '')}
          >
            {word.q}
          </a>
        </li>
      ))}
    </ul>
  )
  function doSearch (e: SyntheticEvent, q: string) {
    e.preventDefault()
    search(q)
  }
}
