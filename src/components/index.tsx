import { useState, useEffect } from 'react'
import { Indexea } from '../openapi'
import { WidgetBean } from '@indexea/sdk'
import { SearchContext, SearchFilter } from '@indexea/sdk/context'
import Home from './home'
import Render from './render'

export type IndexProps = {
  indexea: Indexea
  widget: WidgetBean
}

/**
 * 脚手架的入口，负责保持全局搜索上下文以及执行搜索动作
 * 初始只显示搜索框(home.tsx)，开始搜索时显示搜索结果(render.tsx)
 * @param indexea 接口封装
 * @param widget 组件详情
 * @returns
 */
export default function Index ({ indexea, widget }: IndexProps) {
  //全局搜索上下文
  const [context, setContext] = useState<SearchContext>(
    SearchContext.fromUrl(widget, window.location.search)
  )
  //搜索结果
  const [results, setResults] = useState<any>(null)

  useEffect(() => {
    beginSearch(context)
    doSearch()
  }, [context])

  const render = (
    <Render
      context={context}
      results={results}
      search={search}
      switchQuery={switchQuery}
      sort={sort}
      addFilter={addFilter}
      deleteFilter={deleteFilter}
      changePage={changePage}
      clickHit={clickHit}
    />
  ) //搜索结果渲染页面
  const home = <Home context={context} search={search} /> //默认的搜索首页
  return context.q ? render : home

  function doSearch () {
    if (!context.q) return
    var args: any = context.filters.reduce((obj: any, f: SearchFilter) => {
      obj[f.name] = obj[f.name]
        ? Array.isArray(obj[f.name])
          ? obj[f.name].concat(f.value)
          : [obj[f.name], f.value]
        : f.value
      return obj
    }, {})
    args.sort_by_f = context.sort
    indexea
      .search(
        context.query,
        context.q,
        args,
        (context.page - 1) * context.itemsPerPage,
        context.itemsPerPage
      )
      .then(setResults)
      .catch(console.error)
  }

  function beginSearch (ctx: SearchContext) {
    var newUrl = ''
    if (ctx.q) newUrl += '&q=' + encodeURIComponent(ctx.q)
    //@ts-ignore
    if (ctx.query > 0 && ctx.query != ctx.widget.queries[0].id)
      newUrl += '&i=' + ctx.query
    if (ctx.filters.length > 0)
      newUrl +=
        '&' +
        ctx.filters
          .map(
            f => encodeURIComponent(f.name) + '=' + encodeURIComponent(f.value)
          )
          .join('&')
    if (ctx.page > 1) newUrl += '&p=' + ctx.page
    if (ctx.sort) newUrl += '&sort_by_f=' + encodeURIComponent(ctx.sort)
    newUrl = newUrl ? newUrl.substring(1) : newUrl

    history.pushState({}, '', newUrl ? '?' + newUrl : '/')
  }

  function search (q: string) {
    context.q = q
    context.filters = []
    context.page = 1
    setContext(Object.assign({}, context))
  }

  function switchQuery (query: number) {
    if (context.query != query) {
      context.query = query
      context.filters = []
      setContext(Object.assign({}, context))
    }
  }

  function sort (s: string) {
    context.sort = s
    setContext(Object.assign({}, context))
  }
  function changePage (p: number) {
    context.page = p
    setContext(Object.assign({}, context))
  }

  function addFilter (f: SearchFilter) {
    if (f.multiple || !context.filters.find(sf => sf.name == f.name)) {
      context.filters.push(f)
    } else {
      context.filters = context.filters.map(sf => (sf.name == f.name ? f : sf))
    }
    setContext(Object.assign({}, context))
  }

  function deleteFilter (f: SearchFilter) {
    context.filters = context.filters.filter(
      tf => tf.name != f.name || tf.value != f.value
    )
    setContext(Object.assign({}, context))
  }

  function clickHit (_id: string) {
    indexea.click(results.action, _id)
  }
}
