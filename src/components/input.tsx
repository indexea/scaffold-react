import { FormEvent, SyntheticEvent, useState, useEffect } from 'react'
import { SearchContext } from '@indexea/sdk/context'
import i18n from '../i18n'

export type InputProps = {
  context: SearchContext
  showLogo: boolean
  attrs: any
  search: (q: string) => void
}

/**
 * 搜索输入框组件，实现输入框自动完成的功能
 * @param context
 * @param attrs
 * @param showLogo
 * @param search
 * @returns
 */
export default function ({ context, attrs, showLogo, search }: InputProps) {
  // Logo 的显示由多个因素决定，优先级最高的是 showLogo 参数，其次是 widget 中的设置
  const logo =
    showLogo && (attrs?.logo === undefined || attrs?.logo)
      ? //@ts-ignore
        context?.widget?.settings?.logo || '/static/indexea.png'
      : ''

  const [value, setValue] = useState<string>('')

  useEffect(() => setValue(context.q), [context.q])

  //const value = context.q

  return (
    <form className='d-flex align-items-center mb-3' onSubmit={doSearch}>
      {logo && (
        <div className='logo'>
          <a href='/' onClick={doHome}>
            <img src={logo} className='me-4' alt='logo' height='28' />
          </a>
        </div>
      )}
      <div className='flex-grow-1'>
        <input
          className='form-control'
          value={value}
          autoFocus={true}
          onChange={e => setValue(e.target.value)}
        />
      </div>
      {attrs?.button && (
        <button type='submit' className='btn btn-primary ms-2'>
          {i18n('button')}
        </button>
      )}
    </form>
  )

  function doSearch (e: FormEvent) {
    e.preventDefault()
    search(value)
  }

  function doHome (e: SyntheticEvent) {
    e.preventDefault()
    search('')
  }
}
