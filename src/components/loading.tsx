/**
 * 显示加载中的动画效果
 * @param props
 */
export default function Loading (props: any) {
  return (
    <div
      className='d-flex justify-content-center align-middle'
      style={{ height: '80vh' }}
    >
      <div className='m-auto'>
        <div
          className='spinner-grow spinner-grow-sm text-success'
          role='status'
        >
          <span className='visually-hidden'>Loading...</span>
        </div>
        <div
          className='spinner-grow spinner-grow-sm text-success'
          role='status'
        >
          <span className='visually-hidden'>Loading...</span>
        </div>
        <div
          className='spinner-grow spinner-grow-sm text-success'
          role='status'
        >
          <span className='visually-hidden'>Loading...</span>
        </div>
      </div>
    </div>
  )
}
