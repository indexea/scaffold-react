import { SyntheticEvent } from 'react'

const pagesPerPage = 10

export type PaginationProps = {
  page: number
  totalItems: number
  itemsPerPage: number
  changePage: (p: number) => void
}

/**
 * 显示分页栏
 * @param page
 * @param totalItems
 * @param itemsPerPage
 * @param changePage
 * @returns
 */
export default function Pagination ({
  page,
  totalItems,
  itemsPerPage,
  changePage
}: PaginationProps) {
  const totalPages = Math.ceil(totalItems / itemsPerPage)
  if (totalPages <= 1) return null

  const startPage = page - ((page - 1) % pagesPerPage)
  const endPage =
    startPage + pagesPerPage - 1 > totalPages
      ? totalPages
      : startPage + pagesPerPage - 1

  const pages = Array(endPage - startPage + 1)
    .fill(0)
    .map((_, i) => startPage + i)

  return (
    <ul className='pagination justify-content-center'>
      <li className={startPage <= 1 ? 'page-item disabled' : 'page - item'}>
        <a
          className='page-link'
          href={'?page=' + (startPage - 1)}
          onClick={e => gotoPage(e, startPage - 1)}
        >
          <span aria-hidden='true'>&laquo;</span>
        </a>
      </li>
      {pages.map(p => (
        <li className={page == p ? 'page-item active' : 'page - item'} key={p}>
          <a
            className='page-link'
            href={'?page=' + p}
            onClick={e => gotoPage(e, p)}
          >
            {p}
          </a>
        </li>
      ))}
      <li
        className={
          endPage + 1 >= totalPages ? 'page-item disabled' : 'page-item'
        }
      >
        <a
          className='page-link'
          href={'?page=' + (endPage + 1)}
          aria-label='Next'
          onClick={e => gotoPage(e, endPage + 1)}
        >
          <span aria-hidden='true'>&raquo;</span>
        </a>
      </li>
    </ul>
  )

  function gotoPage (e: SyntheticEvent, p: number) {
    e.preventDefault()
    changePage(p)
  }
}
