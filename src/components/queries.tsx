import { SyntheticEvent } from 'react'
import { QueryBean } from '@indexea/sdk'
import { SearchContext } from '@indexea/sdk/context'

export type QueriesProps = {
  context: SearchContext
  attrs: any
  results: any
  switchQuery: (query: number) => void
}

/**
 * 显示切换不同查询的 Tab 菜单
 * @param context
 * @param attrs
 * @param switchQuery
 */
export default function Queries ({
  context,
  attrs,
  results,
  switchQuery
}: QueriesProps) {
  const queries = context.widget.queries || []
  return (
    <ul className='nav nav-tabs ps-3 mt-3'>
      {queries.map((query: QueryBean) => (
        <li className='nav-item' key={query.id}>
          <a
            href={'?query=' + query.id}
            onClick={e => clickQuery(e, query.id || 0)}
            className={
              query.id == context.query ? 'nav-link active' : 'nav-link'
            }
          >
            {attrs['label' + query.id] || query.name}
            {attrs.show_count && (
              <span className='badge bg-light text-dark rounded-pill fw-light'>
                {results?.results[query.id + ''] || 0}
              </span>
            )}
          </a>
        </li>
      ))}
    </ul>
  )

  function clickQuery (e: SyntheticEvent, query: number) {
    e.preventDefault()
    if (query > 0) switchQuery(query)
  }
}
