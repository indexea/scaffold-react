import { WidgetLayout } from '@indexea/sdk/layout'
import { SearchContext, SearchFilter } from '@indexea/sdk/context'
import Input from './input'
import Hotwords from './hotwords'
import Queries from './queries'
import Aggregations from './aggregations'
import Results from './results'

export type RenderProps = {
  context: SearchContext
  results: any
  search: (q: string) => void
  switchQuery: (query: number) => void
  sort: (s: string) => void
  addFilter: (f: SearchFilter) => void
  deleteFilter: (f: SearchFilter) => void
  changePage: (p: number) => void
  clickHit: (_id: string) => void
}

export default function Render ({
  context,
  results,
  search,
  switchQuery,
  sort,
  addFilter,
  deleteFilter,
  changePage,
  clickHit
}: RenderProps) {
  const layout = WidgetLayout.parse(context.widget.layout || '<layout/>')
  const rows: Element[] = Array.from(layout.rows())

  return (
    <div className='container-fluid p-3'>
      {rows.map((row: any, rindex: number) => (
        <div className='row' key={rindex}>
          {Array.from(row.getElementsByTagName('col')).map(
            (col: any, cindex: number) => (
              <div
                className={'col-sm-' + col.getAttribute('width')}
                key={cindex}
              >
                {Array.from(col.getElementsByTagName('control')).map(
                  (ctrl: any, aindex: number) => (
                    <div className='control' key={aindex}>
                      {ctrl.getAttribute('type') === 'input' && (
                        <Input
                          context={context}
                          showLogo={true}
                          attrs={layout.getAttributes('input')}
                          search={search}
                        />
                      )}
                      {ctrl.getAttribute('type') === 'hotwords' && (
                        <Hotwords
                          context={context}
                          attrs={layout.getAttributes('hotwords')}
                          search={search}
                        />
                      )}
                      {ctrl.getAttribute('type') === 'indices' && (
                        <Queries
                          context={context}
                          attrs={layout.getAttributes('indices')}
                          results={results}
                          switchQuery={switchQuery}
                        />
                      )}
                      {ctrl.getAttribute('type') === 'aggregation' && (
                        <Aggregations
                          context={context}
                          attrs={layout.getAttributes('aggregation')}
                          aggregations={results?.aggregations}
                          addFilter={addFilter}
                        />
                      )}
                      {ctrl.getAttribute('type') === 'results' && (
                        <Results
                          context={context}
                          attrs={layout.getAttributes('results')}
                          results={results}
                          sort={sort}
                          changePage={changePage}
                          addFilter={addFilter}
                          deleteFilter={deleteFilter}
                          clickHit={clickHit}
                          search={search}
                        />
                      )}
                    </div>
                  )
                )}
              </div>
            )
          )}
        </div>
      ))}
    </div>
  )
}
