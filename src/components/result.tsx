import { SyntheticEvent } from 'react'

export type ResultProps = {
  attrs: any
  hit: any
  query: number
  click: (_id: string) => void
}

/**
 * 显示单个搜索结果
 * @param hit
 * @param attrs
 * @param query
 * @param click
 * @returns
 */
export default function Result ({ hit, attrs, query, click }: ResultProps) {
  const fields = { ...hit._source, ...hit.fields }
  const titleField = attrs['title-' + query]
  const title = getFieldValue(fields, titleField)
  const thumbnailField = attrs['thumbnail-' + query]
  const thumbnail = getFieldValue(fields, thumbnailField)
  const urlField = attrs['url-' + query]
  const url = getFieldValue(fields, urlField)
  const outlineField = attrs['outline-' + query]
  const outline = getFieldValue(fields, outlineField)
  const metadatasFields = (attrs['metadatas-' + query] || '').split(',')

  return (
    <div className='d-flex p-0'>
      {thumbnailField && (
        <img className='thumbnail' src={thumbnail} alt='...' />
      )}
      <div className='m-0 p-0 flex-grow-1 row'>
        <h5 className='card-title px-0' v-if='titleField'>
          {url && (
            <a
              href={url}
              rel='noreferrer'
              target='_blank'
              v-if='urlField'
              onClick={e => clickHit(e, hit._id)}
            >
              {hit?.highlight && hit?.highlight[titleField] && (
                <span
                  dangerouslySetInnerHTML={{
                    __html: hit?.highlight[titleField][0]
                  }}
                ></span>
              )}
              {(!hit?.highlight || !hit?.highlight[titleField]) && <>{title}</>}
            </a>
          )}
          {!url && (
            <>
              {hit?.highlight && hit?.highlight[titleField] && (
                <span
                  dangerouslySetInnerHTML={{
                    __html: hit?.highlight[titleField][0]
                  }}
                ></span>
              )}
              {!hit?.highlight || (!hit?.highlight[titleField] && <>{title}</>)}
            </>
          )}
        </h5>
        <div className='card-text px-0'>
          {outlineField && (
            <div className='col-12'>
              {hit?.highlight && hit?.highlight[outlineField] && (
                <span
                  dangerouslySetInnerHTML={{
                    __html: hit?.highlight[outlineField][0]
                  }}
                ></span>
              )}
              {(!hit?.highlight || !hit?.highlight[outlineField]) && (
                <>{outline}</>
              )}
            </div>
          )}
          {metadatasFields && metadatasFields.length > 0 && (
            <div className='col-8 text-muted mt-2'>
              {metadatasFields.map((mfn: string) => (
                <span className='me-2' key={mfn}>
                  {mfn} {getFieldValue(fields, mfn)}
                </span>
              ))}
            </div>
          )}
        </div>
      </div>
    </div>
  )

  function getFieldValue (fields: any, key: string) {
    if (!key) return null
    var v = fields[key]
    if (!v) {
      //nest properties like user.region.city
      var keys = key.split('.')
      for (var i = 0; i < keys.length; i++) {
        v = v ? v[keys[i]] : fields[keys[i]]
        if (typeof v != 'object') {
          break
        }
      }
    }
    return v ? (Array.isArray(v) ? v[0] : v) : ''
  }

  function clickHit (e: SyntheticEvent, _id: string) {
    click(_id)
  }
}
