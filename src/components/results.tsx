import { SearchContext, SearchFilter } from '@indexea/sdk/context'
import i18n from '../i18n'
import Result from './result'
import Pagination from './pagination'
import Tag from './tag'
import SortMenu from './sort'
import Aggregations from './aggregations-dropdown'
import Loading from './loading'

export type ResultsProps = {
  context: SearchContext
  attrs: any
  results: any
  search: (q: string) => void
  sort: (s: string) => void
  addFilter: (f: SearchFilter) => void
  deleteFilter: (f: SearchFilter) => void
  changePage: (p: number) => void
  clickHit: (_id: string) => void
}

/**
 * 搜索结果渲染组件
 * @param context
 * @param attrs
 * @param results
 * @param search
 * @param switchQuery
 * @param sort
 * @param addFilter
 * @param deleteFilter
 * @param changePage
 * @param clickHit
 * @returns
 */
export default function Results ({
  context,
  attrs,
  results,
  search,
  sort,
  addFilter,
  deleteFilter,
  changePage,
  clickHit
}: ResultsProps) {
  if (!results) return <Loading />

  const totalItems = results?.hits?.total.value || 0
  const hits = results?.hits?.hits || []
  const suggests = parseSuggests(results)

  return (
    <div className='results mt-3'>
      {/*显示搜索建议*/}
      {suggests.length > 0 && (
        <small className='d-block text-muted mb-3'>
          {i18n('suggests')}
          {suggests.map(sug => (
            <a
              href={'?q=' + sug}
              className='me-2'
              key={sug}
              onClick={e => {
                e.preventDefault()
                search(sug)
              }}
            >
              {sug}
            </a>
          ))}
        </small>
      )}
      <small className='text-muted d-block mb-1'>
        {totalItems > 0 && (
          <div className='float-end'>
            <SortMenu context={context} sort={sort} />
            <Aggregations
              context={context}
              attrs={attrs}
              aggregations={results?.aggregations}
              addFilter={addFilter}
            />
          </div>
        )}
        <span
          dangerouslySetInnerHTML={{
            __html: i18n('summary', {
              q: context.q,
              total: totalItems,
              took: results?.took || 0
            })
          }}
        ></span>
      </small>
      {/* 显示聚合过滤条件 */}
      {context.filters.length > 0 && (
        <div className='my-2'>
          {context.filters.map(f => (
            <Tag filter={f} deleteFilter={deleteFilter} key={f.value} />
          ))}
        </div>
      )}
      {/* 显示搜索结果 */}
      {hits.map((hit: any) => (
        <div className='card mb-3' key={hit._id}>
          <div className='card-body'>
            <Result
              hit={hit}
              click={clickHit}
              attrs={attrs}
              query={context.query}
            />
          </div>
        </div>
      ))}
      {/* 显示分页条 */}
      <Pagination
        page={context.page}
        itemsPerPage={context.itemsPerPage}
        totalItems={totalItems}
        changePage={changePage}
      />
    </div>
  )
}

function parseSuggests (results: any) {
  var words: string[] = []
  if (results?.suggest) {
    Object.entries(results.suggest).forEach(([k, values]) => {
      ;(values as any).forEach((v: any) => {
        var s = v.options.reduce(
          (a: any, b: any) => {
            if (a.freq && b.freq) return a.freq > b.freq ? a : b
            else return a.score > b.score ? a : b
          },
          { text: '', score: 0, freq: 0 }
        )
        if (s.text) words.push(s.text)
      })
    })
  }
  return words
}
