import { SearchContext } from '@indexea/sdk/context'
import i18n from '../i18n'

export type SortProps = {
  context: SearchContext
  sort: (s: string) => void
}

/**
 * 显示排序菜单
 * @param context
 * @param sort
 */
export default function Sort ({ context, sort }: SortProps) {
  const sorts =
    context.widget.queries?.find(q => q.id === context.query)?.sortFields || []
  const currentSort = sorts?.find(f => f.label === context.sort)

  //if (sorts.length == 0) return null

  return (
    <div className='dropdown d-inline'>
      <a
        className='dropdown-toggle'
        data-bs-placement='left'
        href='/'
        data-bs-toggle='dropdown'
        aria-expanded='false'
      >
        {currentSort?.label || i18n('sort')}
      </a>
      <ul className='dropdown-menu'>
        {sorts.map(s => (
          <li key={s.label}>
            <a
              className={
                context.sort === s.label
                  ? 'dropdown-item active'
                  : 'dropdown-item'
              }
              href='/'
              onClick={e => {
                e.preventDefault()
                sort(s.label || '')
              }}
            >
              {s.label}
            </a>
          </li>
        ))}
        <li>
          <a
            className='dropdown-item'
            href='/'
            onClick={e => {
              e.preventDefault()
              sort('')
            }}
          >
            {i18n('default_sort')}
          </a>
        </li>
      </ul>
    </div>
  )
}
