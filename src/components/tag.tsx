import { SearchFilter } from '@indexea/sdk/context'
import i18n from '../i18n'

export type TagProps = {
  filter: SearchFilter
  deleteFilter: (f: SearchFilter) => void
}

/**
 * 显示聚合的过滤条件
 * @param filter
 * @param deleteFilter
 * @returns
 */
export default function Tag ({ filter, deleteFilter }: TagProps) {
  return (
    <div className='btn-group btn-group-sm me-2'>
      <button className='btn btn-light' disabled>
        {filter.value}
      </button>
      <button
        className='btn btn-light'
        title={i18n('delete_tag')}
        onClick={e => {
          e.preventDefault()
          deleteFilter(filter)
        }}
      >
        &times;
      </button>
    </div>
  )
}
