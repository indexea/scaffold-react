

import en from './locales/en.json'
import cn from './locales/zh-CN.json'

const locale = /zh-CN/i.test(window.navigator.language) ? 'cn' : 'en'
const messages = { cn, en } as Record<string, any>
const i18n = (messages[locale] as Record<string, string>) || {}

export default function (key: string, args: Record<string, any> = {}) {
    args = args || {};
    let template = i18n[key];
    return template?.replace(/\{(\w+)\}/g, function (_s0, s1) {
        return args[s1] || s1;
    });
}