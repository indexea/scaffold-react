import ReactDOM from 'react-dom/client'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import { Indexea } from './openapi'
import config from '../config.json'
import Index from './components/index'
import Error from './components/error'

const indexea = new Indexea(config.api, '', config.widget)
const appDom = document.querySelector('#IndexeaApp')!

indexea
  .widget()
  .then(widget => {
    // 设置站点属性
    document.title = widget.name || 'Indexea'
    if (widget.intro)
      document
        .querySelector('meta[name=description]')
        ?.setAttribute('content', widget.intro)

    // 创建应用，同时指定模版和数据属性
    const app = <Index indexea={indexea} widget={widget} />
    ReactDOM.createRoot(appDom).render(app)
  })
  .catch(e => {
    const err = <Error msg={e.message} />
    ReactDOM.createRoot(appDom).render(err)
  })
